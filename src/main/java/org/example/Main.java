package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.data.FlatModel;
import org.example.dto.RequestModel;
import org.example.manager.RealtyManager;

import java.util.List;

@Slf4j
public class Main {
    public static void main(String[] args) {
        FlatModel flat1 = new FlatModel(4, 23, 3, false, false, 68, true, false, 9900000, true, true, false);
        FlatModel flat2 = new FlatModel(6, 11, 2, false, true, 44, false, true, 4700000, false, true, false);
        FlatModel flat3 = new FlatModel(15, 35, 4, false, false, 134, true, true, 19900000, true, true, false);
        FlatModel flat4 = new FlatModel(1, 5, 1, true, false, 18, false, false, 3560000, true, false, false);
        FlatModel flat5 = new FlatModel(9, 9, 3, false, false, 62, true, false, 7490000, false, true, false);
        FlatModel flat6 = new FlatModel(11, 17, 1, false, false, 34, true, false, 4290000, false, true, false);
        FlatModel flat7 = new FlatModel(3, 3, 2, false, true, 46, false, false, 3290000, true, true, false);
        FlatModel flat8 = new FlatModel(1, 1, 3, false, false, 50, false, false, 7290000, true, true, false);
        FlatModel flat9 = new FlatModel(14, 14, 2, false, false, 57, false, true, 6780000, false, true, false);
        FlatModel flat10 = new FlatModel(5, 6, 1, true, true, 29, false, true, 5000000, true, false, false);

        RequestModel request1 = new RequestModel(1, 19, 1, 37, false, false, 1, 5, false, false, 10, 150, false, false, 1500000, 30900000, true, true);
        RequestModel request2 = new RequestModel(4, 6, 3, 10, false, false, 1, 4, true, true, 15, 95, true, false, 1000000, 9900000, true, true);
        RequestModel request3 = new RequestModel(2, 15, 2, 5, true, false, 1, 3, true, false, 30, 80, false, true, 2000000, 11500000, false, false);

        RealtyManager realtyManager = new RealtyManager();
        realtyManager.createFlat(flat1);
        realtyManager.createFlat(flat2);
        realtyManager.createFlat(flat3);
        realtyManager.createFlat(flat4);
        realtyManager.createFlat(flat5);
        realtyManager.createFlat(flat6);
        realtyManager.createFlat(flat7);
        realtyManager.createFlat(flat8);
        realtyManager.createFlat(flat9);
        realtyManager.createFlat(flat10);

        List<FlatModel> searchResult1 = realtyManager.searching(request1);
        log.debug("searching, result1: {}", searchResult1 + "\n");

        List<FlatModel> searchResult2 = realtyManager.searching(request2);
        log.debug("searching, result2: {}", searchResult2 + "\n");

        int flatsCount = realtyManager.countFlats();
        log.debug("searching, flatsCount: {}", flatsCount + "\n");

    }
}
